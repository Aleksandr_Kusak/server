"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//declare function require(name:string):any;
var interiors_1 = require("./interiors");
var models_1 = require("./models");
var vehicles_1 = require("./vehicles");
var weapons_1 = require("./weapons");
exports.Objects = {
    Interiors: interiors_1.INTERIORS,
    Models: models_1.MODELS,
    Vehicles: vehicles_1.VEHICLES,
    Weapons: weapons_1.WEAPONS
};
