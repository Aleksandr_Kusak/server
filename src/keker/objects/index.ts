//declare function require(name:string):any;
import {INTERIORS} from "./interiors";
import {MODELS} from "./models";
import {VEHICLES} from "./vehicles";
import {WEAPONS} from "./weapons";

export var Objects:objects = {
    Interiors: INTERIORS,
    Models: MODELS,
    Vehicles: VEHICLES,
    Weapons: WEAPONS
}