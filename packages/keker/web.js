"use strict";
var fs = require("fs");
var path = require("path");
var http = require("http");
var url = require("url");
var Web;
(function (Web) {
    var mimeType = {
        '.ico': 'image/x-icon',
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.json': 'application/json',
        '.css': 'text/css',
        '.png': 'image/png',
        '.jpg': 'image/jpeg',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mpeg',
        '.svg': 'image/svg+xml',
        '.pdf': 'application/pdf',
        '.doc': 'application/msword',
        '.eot': 'appliaction/vnd.ms-fontobject',
        '.ttf': 'aplication/font-sfnt'
    };
    http.createServer(function (req, res) {
        res.setHeader('Access-Control-Allow-Origin', '*'); // разрешаем кросс-деменые запросы
        var parsedUrl = url.parse(req.url); // отсекаем от url все лишнее
        var filePath = __dirname + '/ui' + parsedUrl.pathname; // Парсим url в путь к файлу
        var ext = path.extname(filePath); // получаем расширение файла
        if (req.url == "/api/players_list.json") {
            var pl_1 = {
                online: mp.players.length,
                slots: mp.players.size,
                players: []
            };
            mp.players.forEach(function (player) {
                pl_1.players.push({ name: player.name, ip: player.ip, ping: player.ping, id: player.id });
            });
            res.writeHead(200, { 'Content-Type': mimeType['.json'] });
            res.end(JSON.stringify(pl_1), 'utf-8');
        }
        else {
            fs.readFile(filePath, function (error, content) {
                if (error) {
                    if (error.code == 'ENOENT') {
                        res.writeHead(404, { 'Content-Type': 'text/plain' });
                        res.end('404 Not Found');
                    }
                    else {
                        res.writeHead(500);
                        res.end('Error: ' + error.code + ' ..\n');
                    }
                }
                else {
                    res.writeHead(200, { 'Content-Type': mimeType[ext] || 'text/plain' });
                    res.end(content, 'utf-8');
                }
            });
        }
    }).listen(4499); // вешаем наш веб сервер на свободный порт, у меня это 8080
})(Web || (Web = {}));
module.exports = Web;
