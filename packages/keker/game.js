"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var objects_1 = require("./objects");
var utility_1 = require("./utility");
var Game;
(function (Game) {
    Game.start = {
        startspawns: [new mp.Vector3(-425.517, 1123.620, 325.8544), new mp.Vector3(-415.777, 1168.791, 325.854)],
        //startskins: [mp.joaat("a_c_chickenhawk"), mp.joaat("a_c_chop"), mp.joaat("a_c_coyote"), mp.joaat("a_c_husky")]
        startskins: [objects_1.Objects.Models[1], objects_1.Objects.Models[3], objects_1.Objects.Models[6], objects_1.Objects.Models[11]]
    };
    Game.utility = utility_1.Utility;
})(Game = exports.Game || (exports.Game = {}));
