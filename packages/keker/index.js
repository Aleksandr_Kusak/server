"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*import * as fs from "fs";
import * as path from "path";*/
require("./logger");
require("./web");
require("./discord");
var events_1 = require("./events");
var cycle_1 = require("./cycle");
events_1.Events.registerEvents();
cycle_1.Cycle();
