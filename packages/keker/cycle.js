"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// [REAL TIME]
function Cycle() {
    var setTimeTimer = 0; //Таймер, нужен что бы игровое время обновлялось не моментально а с n интервалом
    var hungryTimer = 0;
    setTimeout(function () {
        var date = new Date(); //получаем реальное время
        mp.environment.time.hour = date.getHours(); //устанавливаем игровое время (часы)
        mp.environment.time.minute = date.getMinutes(); // устанавливаем игровое время (минуты)
    }, 0);
    //Игровой цикл
    setInterval(function () {
        setTimeTimer++; //инкрементируем таймер 
        hungryTimer++;
        if (setTimeTimer == 60) {
            //Получаем и устанавливаем время
            var date = new Date();
            mp.environment.time.hour = date.getHours();
            mp.environment.time.minute = date.getMinutes();
            setTimeTimer = 0; //Обнуляем таймер
        }
        // Голод
        if (hungryTimer == 120) {
            mp.players.forEach(function (_player) {
                if (_player.info.auth) {
                    if (_player.info.eat > 0)
                        _player.info.eat--;
                    else
                        _player.health -= 2;
                    var str = new Buffer(JSON.stringify(_player.info)).toString("hex");
                    _player.outputChatBox("<script>server_package_('" + str + "');</script>");
                    _player.outputChatBox("<script>INTERFACE.ui.updateHUD();</script>");
                }
            });
            hungryTimer = 0;
        }
    }, 1000); // 1000 - задержка между следующим вызовом setInterval в мс.
    setInterval(function () {
        // Спидометр
        mp.players.forEach(function (_player) {
            if (_player.vehicle) {
                var ux = _player.vehicle.velocity.x / 1;
                var uy = _player.vehicle.velocity.y / 1;
                var uz = _player.vehicle.velocity.z / 1;
                var u = Math.round(Math.sqrt(Math.pow(ux, 2) + Math.pow(uy, 2) + Math.pow(uz, 2)));
                var speed = Math.round((u / 1000) * 3600);
                var outjson = {
                    incar: true,
                    speed: speed,
                    health: _player.vehicle.engineHealth,
                    body: _player.vehicle.bodyHealth,
                    //model: _player.vehicle.model,
                    petrol: _player.vehicle.info.petrol,
                };
                var str = new Buffer(JSON.stringify(outjson)).toString("hex");
                _player.outputChatBox("<script>server_package('" + str + "', 'speedo');INTERFACE.ui.updateSpeedo();</script>");
            }
        });
    }, 100);
}
exports.Cycle = Cycle;
