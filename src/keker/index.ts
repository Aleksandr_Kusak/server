"use strict";
/*import * as fs from "fs";
import * as path from "path";*/
import "./logger";
import "./web";
import "./discord";
import * as database from "./db";
import {Events} from "./events";
import {Cycle} from "./cycle";

Events.registerEvents();

Cycle();