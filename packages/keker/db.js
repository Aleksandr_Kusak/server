"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mysql = require('mysql');
//Берём конфиг из файла, т.к. данные могут отличатсья для разных машин и т.п.
var db_config = require("../../db_config.json");
var connection;
//Коннект и автоматический реконнект при потере соединения
function Reconnect() {
    connection = mysql.createConnection(db_config);
    connection.connect(function (err) {
        if (err) {
            console.log('Error when connecting to db:', err);
            setTimeout(Reconnect(), 2000);
        }
        else
            console.log("Database connected");
    });
    connection.on('error', function (err) {
        console.log('Db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.log('Database connection lost: reconnecting....');
            Reconnect();
        }
        else {
            throw err;
        }
    });
}
//Коннектимся к базе
Reconnect();
//Добавление игрока в базу
function addPlayerData(playerData, callback) {
    console.log("Adding Player To Database...");
    connection.query("INSERT INTO accounts SET ?", [playerData], function (err, res) {
        if (!err)
            console.log("Adding Player To Database: OK");
        else
            console.log(err);
        if (callback && callback != null)
            callback(err);
    });
}
exports.addPlayerData = addPlayerData;
;
//Загрузка данных игрока из базы
function getPlayerDataBylogin(playerlogin, callback) {
    console.log('Getting player data...');
    connection.query("SELECT * FROM accounts WHERE login = ?", [playerlogin], function (err, res) {
        if (!err) {
            console.log('Getting Player data OK');
            //console.log('Player data: ' + JSON.stringify(res[0]));
        }
        else
            console.log(err);
        if (callback && callback != null) {
            callback(err, res[0]);
        }
    });
}
exports.getPlayerDataBylogin = getPlayerDataBylogin;
;
//Сохранение данных игрока в базу
function setPlayerDataByID(id, playerData, callback) {
    console.log("Saving Player To Database...");
    connection.query("UPDATE accounts SET ? WHERE id = ?", [playerData, id], function (err) {
        if (!err) {
            console.log("Saving Player To Database: OK");
        }
        else
            console.log(err);
        if (callback && callback != null)
            callback(err);
    });
}
exports.setPlayerDataByID = setPlayerDataByID;
;
