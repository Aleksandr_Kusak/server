//const addr = "http://109.195.87.124:4499/"; // Здесь обязательно задаем адресс вашего сервера и порт на котором висит веб
var pos = { x: 0, y: 0 };
var MSG_LOG = ["раз","два","три","четыре","пять"];
var CURR_MSG = null;
var NEW_MSG = "";
var curs = document.getElementById("cursor").style;

function mouse_position(e) {
    pos.x = e.clientX;
    pos.y = e.clientY;
}

setInterval(function() {
    curs.left = pos.x;
    curs.top = pos.y+1;
}, 33);

$('.menubutt_5').hide();

var PLAYER = PLAYER || {};

PLAYER.data = {};
PLAYER.data.player = {
	id: 0,
	auth: false,
	bd_id: 0,
	nick: "",
	access: 0,
	skin: 0,
	position: {x:0.0,y:0.0,z:0.0},
	health: 0,
	armor: 0,
	eat: 0,
	money: 0,
	bank: 0,
	weapons: [{}],
	fraction: 0,
	rank: 0,
	drugs: 0,
	arrests: 0,
	wanted: 0
};
PLAYER.data.vehicle = {
	id: 0,
	model: 0,
	speed: 0,
	incar: false,
	health: 0,
	body: 0,
	fraction: 0,
	petrol: 0,
	owner: 0,
	respawned: false,
	position: {x:0.0,y:0.0,z:0.0},
	bagage: [{}]
};
PLAYER.data.home = {};


var INTERFACE = INTERFACE || {};
INTERFACE.ui = {}; 

INTERFACE = {
	plList: false, // флаг проверки открыто/скрыто окно со списком игроков
	plMenu: false,
	plAuth: false,
	plRegister: false,
	plHud: false,
	plSpeedo: false,
	plHudStyle: 2,
	plChat: false,
	plChatInput: false,
	plChatSize: 0,
	plChatContainer: $("#chat ul#chat_messages"),
	plDebug: false,
	plCursor: false,

 	setShowList: function(show) { this.plList = show; },
	setShowMenu: function(show) { this.plMenu = show; },
	setShowAuth: function(show) { this.plAuth = show; },
	setShowRegister: function(show) { this.plRegister = show; },
	setShowHUD: function(show) { this.plHud = show; },
	setShowSpeedo: function(show) { this.plSpeedo = show;},
	setHUDStyle: function(style) { this.plHudStyle = style; },
	setShowChat: function(show) { this.plChat = show; },
	setShowDebug: function(show) { this.plDebug = show; },
	setShowChatInput: function(show) { this.plChatInput = show; },
	addChatSize: function() { this.plChatSize++; },
	setShowCursor: function(show) { this.plCursor = show; },

	getList: function() { return this.plList; },
	getMenu: function() { return this.plMenu; },
	getAuth: function() { return this.plAuth; },
	getRegister: function() { return this.plRegister; },
	getHUD: function() { return this.plHud; },
	getHUDStyle: function() { return this.plHudStyle; },
	getChat: function() { return this.plChat; },
	getDebug: function() { return this.plDebug; },
	getChatInput: function() { return this.plChatInput; },
	getChatSize: function() { return this.plChatSize; },
	getChatCont: function() { return this.plChatContainer; },
	getCursor: function() { return this.plCursor; },
	getShowedWindows: function() { if (this.getList() || this.getMenu() || this.getAuth() || this.getRegister() || this.getChatInput() ) return false; else return true; }
}

INTERFACE.ui = {
	showList: (show) => {
		INTERFACE.setShowList(show);
		if(show) {
			$('.playerlist').fadeIn("fast");
			INTERFACE.ui.showCursor(show);
		} else {
			$('.playerlist').fadeOut("fast");
			if (INTERFACE.getShowedWindows()) INTERFACE.ui.showCursor(show);
		}
		return true;
	},
	updateList: () => {
		$.getJSON( "api/players_list.json", function( data ) {
			$(".p-list-header p").html("<b><strong>Piter Role-Play</strong> | Игроков онлайн: "+data.online+"/"+data.slots+"</b>"); // кол-во онлайна
			let str = "<tr><th>Ник:</th><th>ID:</th><th>Ping:</th></tr>";
			for(let i = 0; i < data.players.length; i++){
				str += "<tr><td>"+data.players[i].name+"</td><td>"+data.players[i].id+"</td><td>"+data.players[i].ping+"</td>";
			}
			$("#pl-table").html(str);
		});
	},
	showMenu: (show) => {
		INTERFACE.setShowMenu(show);
		if(show) {
			$('.playermenu').fadeIn("fast");
			INTERFACE.ui.showCursor(show);
		} else {
			$('.playermenu').fadeOut("fast");
			if (INTERFACE.getShowedWindows()) INTERFACE.ui.showCursor(show);
		}
		return true;
	},
	openMenuPage: (page) => {
		$("div[id^=menupage]").each(function(){
			var div = $(this);
			var divId = div.attr('id');
			if (divId == "menupage"+page) {
				//$(this).addClass('active');
				$(this).fadeIn("fast");
			} else {
				//$(this).removeClass('active');
				$(this).fadeOut("fast");
			}
		});  
	},
	openMenu: () => {
		$("div[id^=menupage]").each(function(){
			var div = $(this);
			//$(this).removeClass('active');
			$(this).fadeOut("fast");
		});
		
	},
	goToAuth: () => {
		var login = $("input[name=auth_login]").val();
		var password = $("input[name=auth_password]").val();
		if (login.length > 3 && password.length >= 6) {
			var PASSORD_REGEX = "/[a-zA-Z0-9_-]+/g";
			if (password.search(PASSORD_REGEX)) {
				mp.invoke("command", `auth ${login} ${password} panel`);
			} else $(".error-message-auth").html('Не правильный формат Пароля (символы: 1-9 a-Z _ - )');
		} else $(".error-message-auth").html('Не корректные данные');
	},
	goToRegister: () => {
		var login = $("input[name=register_login]").val();
		var password = $("input[name=register_password]").val();
		var password2 = $("input[name=register_password2]").val();
		var nick = $("input[name=register_nickname]").val();
		var email = $("input[name=register_email]").val();
		if (login.length > 3 || login.length < 16) {
			if (password == password2) {
				if (password.length >= 6 || password.length <= 18) {
					var EMAIL_REGEX = "/[a-zA-Z0-9_\./-]+@[a-zA-Z0-9/-]+\.[a-zA-Z\.]+/g";
					var NICK_REGEX = "/[a-zA-Z]+_[a-zA-Z]+/g";
					var PASSORD_REGEX = "/[a-zA-Z0-9_-]+/g";
					if (nick.search(NICK_REGEX)) {
						if (email.search(EMAIL_REGEX)) {
							if (password.search(PASSORD_REGEX)) {
								mp.invoke("command", `register ${login} ${password} ${nick} ${email} panel`);
							} else $(".error-message-register").html('Не правильный формат Пароля (символы: 1-9 a-Z _ - )');
						} else $(".error-message-register").html('Не правильный формат E-mail (Например: email@example.ru)');
					} else $(".error-message-register").html('Не правильный формат ника (Например: Ivan_Ivanov)');
				} else $(".error-message-register").html('Длина пароля от 6 до 18 символов');
			} else $(".error-message-register").html('Пароли не совпадают');
		} else $(".error-message-register").html('Длина логина от 4 до 16 символов');
	},
	goAuth: () => {
		INTERFACE.ui.showRegister(false);
		INTERFACE.ui.showAuth(true);
	},
	goRegister: () => {
		INTERFACE.ui.showAuth(false);
		INTERFACE.ui.showRegister(true);
	},
	showAuth: (show) => {
		INTERFACE.setShowAuth(show);
		if(show) {
			$('.authpanel').fadeIn();
			INTERFACE.ui.showCursor(show);
		} else {
			$('.authpanel').fadeOut();
			if (INTERFACE.getShowedWindows()) INTERFACE.ui.showCursor(show);
		}
		return true;
	},
	showRegister: (show) => {
		INTERFACE.setShowRegister(show);
		if(show) {
			$('.registerpanel').fadeIn();
			INTERFACE.ui.showCursor(show);
		} else {
			$('.registerpanel').fadeOut();
			if (INTERFACE.getShowedWindows()) INTERFACE.ui.showCursor(show);
		}
		return true;
	},
	showHUD: (style) => {
		if (style > 3) style = 1;
		if (style == 1) {
			$('.playerhud').fadeOut("fast");
			INTERFACE.ui.showChat(false);
			INTERFACE.setShowHUD(false);
		} else if (style == 2) {
			$('.playerhud').fadeOut("fast");
			INTERFACE.ui.showChat(true);
			INTERFACE.setShowHUD(true);
		} else if (style == 3) {
			$('.playerhud').fadeIn("fast");
			INTERFACE.ui.showChat(true);
			INTERFACE.setShowHUD(true);
		}
		INTERFACE.setHUDStyle(style);
		return true;
	},
	showSpeedo: (show) => {
		INTERFACE.setShowSpeedo(show);
		if(show) {
			$('.speedo').fadeIn();
		} else {
			$('.speedo').fadeOut();
		}
		return true;
	},
	showDebug: () => {
		var show = !INTERFACE.getDebug();
		INTERFACE.setShowDebug(show);
		if(show) {
			$('#debug_messages').show();
		} else {
			$('#debug_messages').hide();
		}
		return true;
	},
	showChat: (show) => {
		INTERFACE.setShowChat(show);
		if(show) {
			$('#chat_messages').show();
		} else {
			$('#chat_messages').hide();
		}
		return true;
	},
	showChatInput: (show) => {
		INTERFACE.setShowChatInput(show);
		if(show) {
			$('#chat_input').fadeIn("fast");
			$("#chat_msg").focus();
			INTERFACE.ui.showCursor(show);
		} else {
			$("#chat_msg").blur();
			$('#chat_input').fadeOut("fast");
			if (INTERFACE.getShowedWindows()) INTERFACE.ui.showCursor(show);
		}
		return true;
	},
	showCursor: (show) => {
		INTERFACE.setShowCursor(show);
		if(show) {
			$('#cursor').fadeIn("fast");
			mp.invoke("focus", show);
		} else {
			$('#cursor').fadeOut("fast");
			mp.invoke("focus", show);
		}
		return true;
	},

	enableChatInput: (show) => {
		if(show) {
			INTERFACE.ui.showChatInput(show);	
			$("#chat_msg").keydown( function(event) { 
				if(event.which == 13) { 
					var string = $("#chat_msg").val();
					addmess(string);
					console.log("MSG Array:"+MSG_LOG.toString());
					if(string.length > 0) {
						if(string[0] == "/") {
							string = string.substr(1);
							if(string.length > 0) mp.invoke("command", string);
						} else { mp.invoke("chatMessage", string); }				
						$('#chat_msg').val('');
						CURR_MSG = null;
						NEW_MSG = "";
						INTERFACE.ui.showChatInput(false);
						$("#chat_msg").off('keydown');
					}
				}
			});
		} else {
			INTERFACE.ui.showChatInput(show);
		}
	},

	addChatMessage: (str, type, css_json) => {
		if(type != null || css_json != null)
			INTERFACE.getChatCont().prepend("<li data-mtype='" + type + "'>" + str + "</li>").children(":first").css(css_json);
		else
			INTERFACE.getChatCont().prepend("<li>" + str + "</li>");			
		INTERFACE.addChatSize();
		if(INTERFACE.getChatSize() >= 100) { INTERFACE.getChatCont().children(":last").remove(); }
	},

	addScript: (str, type, css_json) => {
		type = "scripts";
		css_json = {"display":"none"};
		$("#scripts").prepend("<li data-mtype='" + type + "'>" + str + "</li>").children(":first").css(css_json);
	},

	addDebugMessage: (str, type, css_json) => {
		$("#debug_messages").prepend("<li data-mtype='" + type + "'>" + str + "</li>").children(":first").css(css_json);		
	},

	showChatMessagesByType(type, show) {
		if(show) {
			INTERFACE.getChatCont().children("data-mtype='" + type + "'").show();
		} else {
			INTERFACE.getChatCont().children("data-mtype='" + type + "'").hide();
		}
	},
	showDebugMessagesByType(type, show) {
		if(show) {
			$("#debug_messages").children("data-mtype='" + type + "'").show();
		} else {
			$("#debug_messages").children("data-mtype='" + type + "'").hide();
		}
	},

	updateInterface() {
		INTERFACE.ui.updatePlayer();
		INTERFACE.ui.updateAdminPanel();
	},
	updatePlayerStats() {

	},
	updateAdminPanel() {
		if (PLAYER.data.player.access >= 6) {
			$('.menubutt_5').show();
		}
	},
	updatePlayer() {
		$(".playernick").html(PLAYER.data.player.nick);
		$(".playerid").html(PLAYER.data.player.id);
		$(".playerwanted").html(PLAYER.data.player.wanted);
		$(".playereat").html(PLAYER.data.player.eat);
		$(".playerhealth").html(PLAYER.data.player.health);
		$(".playerarmor").html(PLAYER.data.player.armor);
		$(".playerfraction").html(PLAYER.data.player.fraction);
		$(".playerrank").html(PLAYER.data.player.rank);
		$(".playercrimes").html(PLAYER.data.player.crimes);
		$(".playerarrests").html(PLAYER.data.player.arrests);
		$(".playermoney").html(PLAYER.data.player.money);
		$(".playerbank").html(PLAYER.data.player.bank);
		$(".playerdrugs").html(PLAYER.data.player.drugs);
		$(".playerweaponlist").html(' ');
		PLAYER.data.player.weapons.forEach( weapon => { $(".playerweaponlist").append("<li class='playerweapon'>Оружие: <b>" + weapon.name + "</b> | Патроны: "+weapon.ammo+"</li>"); });
	},
	updateSpeedo() {
		if (PLAYER.data.vehicle.incar) {
			//$(".speedo-model-name").html(PLAYER.data.vehicle.model);
			$(".speedo-speed").html(PLAYER.data.vehicle.speed);
			$(".needle").css({"transform":"rotate("+PLAYER.data.vehicle.speed+"deg)"});
			$(".speedo-petrol").html(PLAYER.data.vehicle.petrol);
			$(".speedo-engine").html(Math.round(PLAYER.data.vehicle.health/10));
			$(".speedo-body").html(Math.round(PLAYER.data.vehicle.body/10));
			if (PLAYER.data.vehicle.petrol <= 10) {
				$(".speedo-signal-petrol").css({"display":"inline-block"});
			} else {
				$(".speedo-signal-petrol").css({"display":"none"});
			}
			if (PLAYER.data.vehicle.health <= 300) {
				$(".speedo-signal-engine").css({"display":"inline-block"});
			} else {
				$(".speedo-signal-engine").css({"display":"none"});
			}
			if (PLAYER.data.vehicle.body <= 300) {
				$(".speedo-signal-body").css({"display":"inline-block"});
			} else {
				$(".speedo-signal-body").css({"display":"none"});
			}
		}
	},
	updateHUD() {
		$(".HUD-money-count").html(PLAYER.data.player.money);
		$(".HUD-eat-count").html(PLAYER.data.player.eat);
	}
};

INTERFACE.ui.addChatMessage("Добро пожаловать на сервер <b>Piter-Role Play</b>", "system", {"color":"#348EE1"});
INTERFACE.ui.addChatMessage("Зарегистрируйтесь или Авторизируйтесь на сервере", "system", {"color":"#348EE1"});
INTERFACE.ui.addChatMessage("<b>Авторизация/Регистрация:</b> - нажмите на кнопку F2", "system", {"color":"yellow"});
INTERFACE.ui.addChatMessage("Или используйте команды <b>/register</b> и <b>/auth</b>", "system", {"color":"yellow"});
INTERFACE.ui.showChat(true);

$("body").keydown(function( e ) { // событие нажания на кнопку
	if(e.which == 112){ // 9 KeyCode кнопки TAB
		if(INTERFACE.getList()){ // если модальное окно открыто скрываем его
			INTERFACE.ui.showList(false);
		} else { 
			INTERFACE.ui.updateList();
			INTERFACE.ui.showList(true);
		}
	} else if (e.which == 113) {
		if (!PLAYER.data.player.auth) {
			if(INTERFACE.getAuth()) {
				INTERFACE.ui.showAuth(false);
			} else if (INTERFACE.getRegister()) {
				INTERFACE.ui.showRegister(false);
			} else {
				INTERFACE.ui.showAuth(true);
			}
		}
	} else if (e.which == 114) {
		if (PLAYER.data.player.auth) {
			if(INTERFACE.getMenu()) {
				INTERFACE.ui.showMenu(false);
			} else {
				INTERFACE.ui.showMenu(true);
			}
		}
	} else if ( e.which == 117) {
		if (!INTERFACE.getChatInput()) {
			INTERFACE.ui.enableChatInput(true);
		} else {
			INTERFACE.ui.enableChatInput(false);
		}
	} else if (e.which == 38) {
		// Вверх
		if (CURR_MSG == null) {
			NEW_MSG = $("#chat_msg").val();
			CURR_MSG = 0;
		} else if (CURR_MSG < MSG_LOG.length-1) {
			CURR_MSG++;
		}
		changemess(CURR_MSG);
	} else if (e.which == 40) {
		// Вниз
		if (CURR_MSG != null) {
			if (CURR_MSG > 0) {
				CURR_MSG--;
				changemess(CURR_MSG);
			} else {
				$("#chat_msg").val(NEW_MSG);
				CURR_MSG = null;
			}
		}
	}
	else if (e.which == 118) {
			INTERFACE.ui.showHUD(INTERFACE.getHUDStyle()+1);
	}
});




function insertMessageToChat(str, type, css_json) {
	if (str.indexOf('<script>') != -1) INTERFACE.ui.addScript(str,type,css_json);
	else if (str.indexOf('[DEBUG]') != -1) INTERFACE.ui.addDebugMessage(str,type,css_json);
	else if (str != null && str.length > 1) INTERFACE.ui.addChatMessage(str,type,css_json);
}

function hex2a(hexx) {
    var hex = hexx.toString();
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function changemess(element) {
	console.log("MSG_LOG["+element+"]");
	$("#chat_msg").val(MSG_LOG[element]);
}

function addmess(str) {
	console.log("MSG_LOG[0] = "+str);
	if(MSG_LOG.length > 10) MSG_LOG.slice(0,10);
    MSG_LOG.unshift(str);
}

function server_package(json, object) {
	if (object == "player") {
		let objectPlayer = {};
		objectPlayer = JSON.parse(hex2a(json));

		if (objectPlayer.id) PLAYER.data.player.id = objectPlayer.id;
		if (objectPlayer.auth) PLAYER.data.player.auth = objectPlayer.auth;
		if (objectPlayer.bd_id) PLAYER.data.player.bd_id = objectPlayer.bd_id;
		if (objectPlayer.nick) PLAYER.data.player.nick = objectPlayer.nick;
		if (objectPlayer.access) PLAYER.data.player.access = objectPlayer.access;
		if (objectPlayer.skin) PLAYER.data.player.skin = objectPlayer.skin;
		if (objectPlayer.position) PLAYER.data.player.position = objectPlayer.position;
		if (objectPlayer.health) PLAYER.data.player.health = objectPlayer.health;
		if (objectPlayer.armor) PLAYER.data.player.armor = objectPlayer.armor;
		if (objectPlayer.eat) PLAYER.data.player.eat = objectPlayer.eat;
		if (objectPlayer.money) PLAYER.data.player.money = objectPlayer.money;
		if (objectPlayer.bank) PLAYER.data.player.bank = objectPlayer.bank;
		if (objectPlayer.weapons) PLAYER.data.player.weapons = objectPlayer.weapons;
		if (objectPlayer.fraction) PLAYER.data.player.fraction = objectPlayer.fraction;
		if (objectPlayer.rank) PLAYER.data.player.rank = objectPlayer.rank;
		if (objectPlayer.drugs) PLAYER.data.player.drugs = objectPlayer.drugs;
		if (objectPlayer.arrests) PLAYER.data.player.arrests = objectPlayer.arrests;
		if (objectPlayer.wanted) PLAYER.data.player.wanted = objectPlayer.wanted;
	} else if (object == "vehicle") {
		let objectVehicle = {};
		objectVehicle = JSON.parse(hex2a(json));

		if (objectVehicle.id) PLAYER.data.vehicle.id = objectVehicle.id;
		if (objectVehicle.model) PLAYER.data.vehicle.model = objectVehicle.model;
		if (objectVehicle.health) PLAYER.data.vehicle.health = objectVehicle.health;
		if (objectVehicle.fraction) PLAYER.data.vehicle.fraction = objectVehicle.fraction;
		if (objectVehicle.petrol) PLAYER.data.vehicle.petrol = objectVehicle.petrol;
		if (objectVehicle.owner) PLAYER.data.vehicle.owner = objectVehicle.owner;
		if (objectVehicle.respawned) PLAYER.data.vehicle.respawned = objectVehicle.respawned;
		if (objectVehicle.position) PLAYER.data.vehicle.position = objectVehicle.position;
		if (objectVehicle.bagage) PLAYER.data.vehicle.bagage = objectVehicle.bagage;

	} else if (object == "home") {
		var objectHome = {};
		objectHome = JSON.parse(hex2a(json));
		PLAYER.data.home = objectHome;

	} else if (object == "speedo") {
		var objectVehicle = {};
		objectVehicle = JSON.parse(hex2a(json));
		if (objectVehicle.id) PLAYER.data.vehicle.id = objectVehicle.id;
		if (objectVehicle.model) PLAYER.data.vehicle.model = objectVehicle.model;
		if (objectVehicle.incar) PLAYER.data.vehicle.incar = objectVehicle.incar;
		if (objectVehicle.speed) PLAYER.data.vehicle.speed = objectVehicle.speed;
		if (objectVehicle.petrol) PLAYER.data.vehicle.petrol = objectVehicle.petrol;
		if (objectVehicle.health) PLAYER.data.vehicle.health = objectVehicle.health;
		if (objectVehicle.body) PLAYER.data.vehicle.body = objectVehicle.body;
	}
}