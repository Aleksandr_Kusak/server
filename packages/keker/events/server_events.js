"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.serverEvents = {
    "playerJoin": function (player) {
        player.info = {
            auth: false,
            id: 0,
            access: 0,
            eat: 0,
            money: 0,
            bank: 0,
            weapons: [],
            arrests: 0,
            stars: 0,
            fraction: 0,
            rank: 0,
            drugs: 0
        };
        player.outputChatBox("<script>window.location = 'http://94.19.127.94:4499/interface.html';</script>");
        /*var playerData = {
            name : mp.players[data.id].name,
            id : mp.players[data.id].id,
            position : mp.players[data.id].position,
            health : mp.players[data.id].health,
            armor : mp.players[data.id].armour,
            skin : mp.players[data.id].model,
            info : mp.players[data.id].info
        }*/
        player.model = game.start.startskins[Math.floor(Math.random() * game.start.startskins.length)];
        //player.info.weapons.forEach( weapon => { player.giveWeapon(mp.joaat(weapon.name), weapon.ammo); })
        player.spawn(game.start.startspawns[Math.floor(Math.random() * game.start.startspawns.length)]);
    },
    "playerDeath": function (player, reason, killer) {
        if (player.info.auth) {
            player.spawn(game.start.startspawns[Math.floor(Math.random() * game.start.startspawns.length)]);
        }
        else {
            player.info.skin = game.start.startskins[Math.floor(Math.random() * game.start.startskins.length)];
            player.model = player.info.skin;
            player.spawn(game.start.startspawns[Math.floor(Math.random() * game.start.startspawns.length)]);
        }
    },
    "playerSpawn": function (player) {
        player.outputChatBox('Игрок <b>' + player.info.name + '</b> был заспавнен');
    },
    "playerQuit": function (player, reason, kickReason) {
        //dimensions[player.dimension]--;
        DB.connection.query("UPDATE accounts SET skin = ?,health = ?,armor = ?,eat = ?,money = ?,bank = ?,weapons = ?,arrests = ?,stars = ?,fraction = ?,rank = ?,drugs = ?,posx = ?,posy = ?,posz = ? WHERE id = ?", [player.info.skin, player.info.health, player.info.armor, player.info.eat, player.info.money, player.info.bank, JSON.stringify(player.info.weapons), player.info.arrests, player.info.stars, player.info.fraction, player.info.rank, player.info.drugs, player.position.x, player.position.y, player.position.z, player.info.id]);
        var str = player.name + " вышел";
        game.utility.proximityMessage(25.0, player, str);
        player.info = {};
    },
    "playerChat": function (player, text) {
        var str = player.name + "<b>(" + player.id + ")</b>: " + striptags(text, '<b><s><u><strong>');
        if (player.info.auth)
            game.utility.proximityMessage(25.0, player, str);
        else
            game.utility.proximityMessage(25.0, player, player.name + "<b>(" + player.id + ")</b>: *голос животного");
    },
    "playerCommand": function (player, cmdtext) {
        var arr = cmdtext.split(" ");
        var cmd = commands[arr[0]];
        console.log(cmdtext);
        if (cmd != null) {
            cmd(player, arr, cmdtext);
        }
    },
    "playerEnteredVehicle": function (player, vehicle) {
        console.log(player.vehicle.id);
    },
    "playerEnterVehicle": function (player, vehicle) {
        console.log(player.vehicle.id);
    },
    "playerLeftVehicle": function (player, vehicle) {
        console.log(player.vehicle.id);
    },
    "playerExitVehicle": function (player, vehicle) {
        console.log(player.vehicle.id);
    }
};
